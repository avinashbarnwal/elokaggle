import numpy as np
import pandas as pd
import datetime
import gc
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import lightgbm as lgb
from   sklearn.linear_model import BayesianRidge
from   sklearn.model_selection import StratifiedKFold,RepeatedKFold
from   sklearn.metrics import mean_squared_error
from   sklearn.ensemble import RandomTreesEmbedding
import utils
import tqdm
import pickle
import multiprocessing as mp
import time
import math
import warnings
import matplotlib.gridspec as gridspec
import scipy.stats as st
import statsmodels as sm
warnings.filterwarnings('ignore')

from scipy.stats import ks_2samp
np.random.seed(0)

#matplotlib.rcParams['figure.figsize'] = (20, 5)

matplotlib.style.use('ggplot')


def get_feature_importances(data, run_cnt, shuffle, seed=None):
    
    # Gather real features
    
    train_columns = [c for c in data.columns if c not in ['card_id', 'first_active_month',
                                                          'target','outliers']]
    target        = data['target']


    # Go over fold and keep track of CV score (train and valid) and feature importances
    
    # Shuffle target if required
    y = target.copy()
    if shuffle:
        # Here you could as well use a binomial distribution
        y = data['target'].copy().sample(frac=1.0)
    
    # Fit LightGBM in RF mode, yes it's quicker than sklearn RandomForest
    dtrain = lgb.Dataset(data[train_columns], y, free_raw_data=False, silent=True)
    
    param  = {'num_leaves': 40,
              'min_data_in_leaf': 30, 
              'objective':'regression',
              'max_depth': 6,
              'learning_rate': 0.015,
              'min_child_samples': 20,
             'boosting_type': 'gbdt',
             'feature_fraction': 0.6,
             'bagging_freq': 1,
             'bagging_fraction': 0.8,
             'bagging_seed': 11,
             'metric': 'rmse',
             'lambda_l1': 0.1,
             'verbosity': -1,
             'nthread': 4,
             'n_estimators': 400,
             'random_state': 0}
    
    # Fit the model
    clf = lgb.train(params=param,
                    train_set=dtrain,
                   # categorical_feature=categorical_feats
                   )

    # Get feature importances
    imp_df                     = pd.DataFrame()
    imp_df["feature"]          = list(train_columns)
    imp_df["importance_gain"]  = clf.feature_importance(importance_type='gain')
    imp_df["importance_split"] = clf.feature_importance(importance_type='split')
    imp_df['trn_score']        = mean_squared_error(clf.predict(data[train_columns]), y)**0.5
    imp_df['run']              = run_cnt
    
    return imp_df

def creation_null_imp_data(df_train):
    
    #actual_imp_df.sort_values('importance_gain', ascending=False)[:10]
    parent_data = {}
    runs = 100
    for i in range(runs):
        parent_data[i] = df_train



    #res        = []
    start      = time.time()
    mp_pool    = mp.Pool()
    runs       = 100

    input_data = np.arange(0,runs)
    #res1.extend(mp_pool.starmap(test,zip(input_data)))
    res1       = mp_pool.starmap(get_feature_importances,zip(parent_data.values(),
                                                             parent_data.keys(),np.repeat(True,runs)))
    mp_pool.close()
    mp_pool.join()
    time_taken = (time.time()-start)/60
    
    print(time_taken)


    null_imp_df = pd.DataFrame()

    for i in range(runs):
        null_imp_df = pd.concat([null_imp_df,res1[i]],axis=0)
        
    return null_imp_df


def output_null_data():
    path = '../data/output/feature_null_importance'
    utils.to_pickles(null_imp_df,path, split_size = 5, inplace=True)

if __name__ == '__main__':

	print('Reading files')
	df_train         = pd.read_pickle("../data/input/train_test/train_final.pkl")
	df_test          = pd.read_pickle("../data/input/train_test/test_final.pkl")
	print('Files read in memory')

	print('Finding the actual distribution of features')
	actual_imp_df = get_feature_importances(data=df_train,run_cnt=0, shuffle=False)
	print('actual importance df generated')

	print('Finding the null importance')
	null_imp_df = creation_null_imp_data(df_train)
	output_null_data()
	print('Null importance saved')
