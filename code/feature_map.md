Feature map

Legend:
features written inside () are not yet implemented
*** are things that need to be implemented

Note:
The categorical features(from train.csv) are not changed, for LGBM its better to keep it that way
Convert data into lgb.dataset format



*** Try the Kfold Split for model tuning in LGBM
*** Catboost supposed to work better with categorical features, check
*** model blending with other models ( own or any of the top uploaded kaggler's)
*** create features for merchants and map it back to train csv via transactions merchant_id
*** Look for DL solutions



I)train.csv
1) feature_1,feature_2 and feature_3  kept unchanged
2) converted first active month into month, year



historical_transactions.csv
grouped all the items by card_id
1) month_lag ---- mode and mean of month lag, min, max
2) purchase_date ---- month, year, date, hour features
3) authorized flag --- count of two levels Y and N , mean
4) category_3 --- count of each level   [178159 missing values]
5) installments --- mean, count, nunique , sum, median, max, min, std
6) category_1 --- count of each level, mean
7) merchant_category_id --- most frequent item and nunique
8) merchant_id --- most frequent item and nunique [138481 missing values]
9) purchase_amount --- mean, min and max, sum,median, std
10) city_id --- most frequent item and nunique
11) state_id --- most frequent item and nunique
12) category_2 --- count of each level, sum, mean [2652864 missing values] (nunique for na)
13) subsector_id ---- (nunique)


new_merchant_transactions.csv (same as historical)
(above features are replicated)



merchants.csv

merchant_id
merchant_group_id
merchant_category_id
subsector_id
numerical_1
numerical_2
category_1
most_recent_sales_range
most_recent_purchases_range
avg_sales_lag3 [13 missing values]
avg_purchases_lag3
active_months_lag3
avg_sales_lag6 [13 missing values]
avg_purchases_lag6
active_months_lag6
avg_sales_lag12 [13 missing values]
avg_purchases_lag12
active_months_lag12
category_4
city_id
state_id
category_2  [11887 missing values]